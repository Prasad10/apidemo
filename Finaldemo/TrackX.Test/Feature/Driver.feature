﻿Feature: Driver Resource

Background:
Given I am a client

Scenario: Get All Drivers By Source Id
	When I make GET Request '/drivers/source/1'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"D1","phoneNo":1234567890,"locationId":1}]'

Scenario: Get Driver By Id
	When I make GET Request '/drivers/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"D1","phoneNo":1234567890,"locationId":1}'
