﻿Feature: Location Resource

Background:
Given I am a client

Scenario: Get All Locations
	When I make GET Request '/locations'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"L1"},{"id":2,"name":"L2"}]'

