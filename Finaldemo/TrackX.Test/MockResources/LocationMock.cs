﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackX.Model.DB;
using TrackX.Repository.Interfaces;
using Moq;

namespace TrackX.Test.MockResources
{
    class LocationMock
    {
        public static readonly Mock<ILocationRepository> LocationRepoMock = new Mock<ILocationRepository>();

        public static readonly List<Location> ListOfLocations = new List<Location>
        {
            new Location
            {
                Id = 1,
                Name = "L1",
            },
            new Location
            {
                Id = 2,
                Name = "L2",
            }
        };

        public static void MockGetAll()
        {
            LocationRepoMock.Setup(x => x.Get()).Returns(ListOfLocations);

        }
        public static void MockGetById()
        {
            /*LocationRepoMock.Setup(x => x.Get(It
                .IsAny<int>())).Returns((int i) => ListOfLocations.First(x => x.Id == i));*/
        }
        public static void MockPost()
        {
            
        }
        public static void MockPut()
        {
            
        }
        public static void MockDelete()
        {
           
        }
    }
}
