﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using TrackX.Model.DB;
using TrackX.Repository.Interfaces;

namespace TrackX.Test.MockResources
{
    class DriverMock
    {
        public static readonly Mock<IDriverRepository> DriverRepoMock = new Mock<IDriverRepository>();

        public static readonly List<Driver> ListOfDrivers= new List<Driver>
        {
            new Driver
            {
                Id = 1,
                Name = "D1",
                PhoneNo=1234567890,
                LocationId=1
            },
            new Driver
            {
                Id = 2,
                Name = "D2",
                PhoneNo=1234567890,
                LocationId=2
            }
        };

        public static void MockGetDriversBySrcId()
        {
            DriverRepoMock.Setup(x => x.GetDriversBySrcId(It.IsAny<int>())).Returns((int i)=>ListOfDrivers.Where(x=>x.LocationId==i));

        }
        public static void MockGetById()
        {
            DriverRepoMock.Setup(x => x.Get(It
                .IsAny<int>())).Returns((int i) => ListOfDrivers.First(x => x.Id == i));
        }
        public static void MockPost()
        {

        }
        public static void MockPut()
        {

        }
        public static void MockDelete()
        {

        }
    }
}
