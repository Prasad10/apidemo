﻿using TrackX.Test.MockResources;
using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;
namespace TrackX.Test.StepFiles
{
    [Scope(Feature = "Driver Resource")]
    [Binding]
    class DriverSteps : BaseSteps
    {
        public DriverSteps(CustomWebApplicationFactory factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(_ => DriverMock.DriverRepoMock.Object);
                });
            }))
        {

        }
        [BeforeScenario]
        public static void Mocks()
        {
            DriverMock.MockGetDriversBySrcId();
            DriverMock.MockGetById();
        }
    }
}
