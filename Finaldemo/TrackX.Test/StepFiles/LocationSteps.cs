﻿using TrackX.Test.MockResources;
using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;
namespace TrackX.Test.StepFiles
{
    [Scope(Feature = "Location Resource")]
    [Binding]
    class LocationSteps : BaseSteps
    {
        public LocationSteps(CustomWebApplicationFactory factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(_ => LocationMock.LocationRepoMock.Object);
                });
            }))
        {

        }
        [BeforeScenario]
        public static void Mocks()
        {
            LocationMock.MockGetAll();
            LocationMock.MockGetById();
        }
    }
}
