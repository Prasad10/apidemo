using System.Collections.Generic;
using TrackX.Model.Response;
namespace TrackX.Service
{
	public interface ILocationService
    {
        public IEnumerable<LocationResponse> Get();

    }
}