﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Model.Response;

namespace TrackX.Service
{
    public interface IUserService
    {
        public UserResponse GetById(int id);
    }
}
