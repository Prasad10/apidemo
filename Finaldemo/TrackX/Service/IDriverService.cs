﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Model.Response;

namespace TrackX.Service
{
    public interface IDriverService
    {
        public IEnumerable<DriverResponse> GetDriversBySourceId(int id);
        public DriverResponse Get(int id);
    }
}
