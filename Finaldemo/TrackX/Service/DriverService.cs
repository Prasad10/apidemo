﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Model.Response;
using TrackX.Repository.Interfaces;

namespace TrackX.Service
{
    public class DriverService : IDriverService
    {
        private readonly IDriverRepository _driverRepository;
        public DriverService(IDriverRepository driverRepository)
        {
            _driverRepository = driverRepository;
        }
        public IEnumerable<DriverResponse> GetDriversBySourceId(int srcId)
        {
            var drivers=_driverRepository.GetDriversBySrcId(srcId);
            return drivers.Select(x => new DriverResponse
            {
                Id = x.Id,
                Name = x.Name,
                PhoneNo = x.PhoneNo,
                LocationId = x.LocationId
            });
        }
        public DriverResponse Get(int id)
        {
            var x = _driverRepository.Get(id);
            if(x is null)
            {
                return null;
            }
           return  new DriverResponse
            {
                Id = x.Id,
                Name = x.Name,
                LocationId = x.LocationId,
                PhoneNo = x.PhoneNo
            };
        }
    }
}
