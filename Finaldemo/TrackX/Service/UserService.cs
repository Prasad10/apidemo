﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Model.Response;
using TrackX.Repository.Interfaces;

namespace TrackX.Service
{

    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public UserResponse GetById(int id)
        {
            var user = _userRepository.GetById(id);
            if(user is null)
            {
                return null;
            }
            
            return new UserResponse
            {
                Id = user.Id,
                Name = user.Name,
                PhoneNo=user.PhoneNo
            };
        }
    }
}
