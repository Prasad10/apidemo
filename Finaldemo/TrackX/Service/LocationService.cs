using System.Collections.Generic;
using TrackX.Model.Response;
using TrackX.Repository.Interfaces;
using System.Linq;
using TrackX.Model.DB;
using TrackX.Model.Request;
using TrackX.Service;
namespace TrackX.Service
{
    public class LocationService : ILocationService 
    {
        private readonly ILocationRepository _locationRepository;
        public LocationService(ILocationRepository locationRepository)
        {
            _locationRepository = locationRepository;
        }
        public IEnumerable<LocationResponse> Get() 
        {
            var locations = _locationRepository.Get();
            return locations.Select(l => new LocationResponse
            {
                Id = l.Id,
                Name = l.Name
            });
        }
    }

}