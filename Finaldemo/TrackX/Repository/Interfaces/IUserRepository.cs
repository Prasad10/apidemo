﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Model.DB;

namespace TrackX.Repository.Interfaces
{
    public interface IUserRepository
    {
        public User GetById(int id);
    }
}
