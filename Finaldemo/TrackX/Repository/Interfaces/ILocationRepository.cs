using System.Security.AccessControl;
using System;
using System.Collections.Generic;
using TrackX.Model.DB;
using TrackX.Model.Request;
namespace TrackX.Repository.Interfaces
{
    public interface ILocationRepository{
        public IEnumerable<Location> Get();
    }
}