﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using TrackX.Model.DB;
using TrackX.Repository.Interfaces;

namespace TrackX.Repository.Classes
{
    public class UserRepository : IUserRepository
    {
        private readonly Connection _connection;
        public UserRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
        }
        public User GetById(int id)
        {
            const string sql = @"SELECT * FROM Users WHERE Id=id";
            using(var connection=new SqlConnection(_connection.DB))
            {
                var user = connection.QueryFirstOrDefault<User>(sql);
                return user;
            }
        }
    }
}
