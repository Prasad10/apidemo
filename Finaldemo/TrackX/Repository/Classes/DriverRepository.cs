﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using TrackX.Model.DB;
using TrackX.Repository.Interfaces;

namespace TrackX.Repository.Classes
{
    public class DriverRepository : IDriverRepository
    {
        private readonly Connection _connection;
        public DriverRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
        }
        public Driver Get(int id)
        {
            const string sql = @"SELECT * FROM Drivers WHERE Id=id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                var driver= connection.QueryFirstOrDefault<Driver>(sql);
                return driver;
            }

        }
        public IEnumerable<Driver> GetDriversBySrcId(int srcId)
        {
            const string sql = @"SELECT D.*
                                      FROM Drivers D JOIN Source_Destination SD 
                                      ON D.LocationId=SD.SourceId JOIN Riding R 
                                      ON R.SrcDestId=SD.Id WHERE 
                                      SD.Distance<6 AND 
                                      R.Status<>'Started'
                                      AND D.LocationId=srcId";
            using (var connection = new SqlConnection(_connection.DB))
            {
                var drivers = connection.Query<Driver>(sql);
                return drivers;
            }

        }
    }
}
