using System.Collections;
using System.Data.Common;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Options;
using TrackX.Repository.Interfaces;
using TrackX.Model.DB;
namespace TrackX.Repository.Classes
{
    public class LocationRepository:ILocationRepository
    {
        private readonly Connection _connection;
        public LocationRepository(IOptions<Connection> connection)
        {
            _connection=connection.Value;
        }
        public IEnumerable<Location> Get()
        {
            const string sql=@"SELECT * FROM Location";
            using (var connection=new SqlConnection(_connection.DB)){
                var locations=connection.Query<Location>(sql);
                return locations;
            }
        }
    }
}