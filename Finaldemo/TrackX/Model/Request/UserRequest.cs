﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackX.Model.Request
{
    public class UserRequest
    {
        public string Name { get; set; }
        public int PhoneNo { get; set; }
    }
}
