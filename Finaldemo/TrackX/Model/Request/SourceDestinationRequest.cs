﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackX.Model.Request
{
    public class SourceDestinationRequest
    {
        public int SrcId { get; set; }
        public int DestId { get; set; }
        public int Distance { get; set; }
    }
}
