﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackX.Model.Request
{
    public class RidingRequest
    {
        public int UserId { get; set; }
        public int DriverId { get; set; }
        public int Price { get; set; }
        public string Status { get; set; }
        public int SrcDestId { get; set; }
    }
}
