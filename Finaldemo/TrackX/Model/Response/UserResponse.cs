﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackX.Model.Response
{
    public class UserResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PhoneNo { get; set; }
    }
}
