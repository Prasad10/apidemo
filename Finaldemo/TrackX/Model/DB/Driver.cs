using System;
namespace TrackX.Model.DB
{
    public class Driver
    {
        public int Id { get; set; }
        public string Name { get; set; }     
        public int PhoneNo { get; set; }
        public int LocationId { get; set; }
    }
}