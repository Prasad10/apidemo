using System.Security.Cryptography.X509Certificates;
using System;
namespace TrackX.Model.DB
{
    public class User
    {
        public int Id { get; set; }
        public string Name  { get; set; }         
        public int PhoneNo { get; set; }
    }
}