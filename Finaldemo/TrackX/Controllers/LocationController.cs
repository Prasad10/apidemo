using System.Collections.Generic;
using TrackX.Model.DB;
using TrackX.Model.Request;
using TrackX.Service;
using Microsoft.AspNetCore.Mvc;

namespace TrackX.Controllers
{
    [ApiController]
    [Route("locations")]
    public class LocationController : ControllerBase
    {
        private readonly ILocationService _locationService;

        public LocationController(ILocationService locationService)
        {
            _locationService = locationService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var locations = _locationService.Get();
            return Ok(locations);
        }
    }
}