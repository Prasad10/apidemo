﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TrackX.Service;

namespace TrackX.Controllers
{
    [ApiController]
    [Route("drivers")]
    public class DriverController:ControllerBase
    {
        private readonly IDriverService _driverService;
        public DriverController(IDriverService driverService)
        {
            _driverService = driverService;
        }
        [HttpGet("{id}")]

        public IActionResult Get(int id)
        {
            var driver = _driverService.Get(id);
            return Ok(driver);
        }
        [HttpGet("source/{srcId}")]
        public IActionResult GetDriversBySrcId(int srcId)
        {
            var drivers = _driverService.GetDriversBySourceId(srcId);
            return Ok(drivers);
        }
    }
}
