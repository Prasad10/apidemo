CREATE DATABASE TrackX

USE TrackX

CREATE TABLE Location (
	Id INT PRIMARY KEY IDENTITY(1, 1)
	,Name VARCHAR(100)
	)
INSERT INTO Location(Name) VALUES('Hyderabad')

INSERT INTO Drivers(Name,PhoneNo,LocationId) VALUES('Driver1',1234567890,1)

SELECT * FROM Drivers
CREATE TABLE Users (
	Id INT PRIMARY KEY IDENTITY(1, 1)
	,Name VARCHAR(100)
	,PhoneNo INT NOT NULL
	)

CREATE TABLE Drivers (
	Id INT PRIMARY KEY IDENTITY(1, 1)
	,Name VARCHAR(100)
	,PhoneNo INT NOT NULL
	,LocationId INT FOREIGN KEY REFERENCES Location(Id)
	)

CREATE TABLE Source_Destination (
	Id INT PRIMARY KEY IDENTITY(1, 1)
	,SourceId INT FOREIGN KEY REFERENCES Location(Id)
	,DestinationId INT FOREIGN KEY REFERENCES Location(Id)
	,Distance INT
	)

CREATE TABLE Riding (
	Id INT PRIMARY KEY IDENTITY(1, 1)
	,SrcDestId INT FOREIGN KEY REFERENCES Source_Destination(Id)
	,UserId INT FOREIGN KEY REFERENCES Users(Id)
	,DriverId INT FOREIGN KEY REFERENCES Drivers(Id)
	,Status VARCHAR(10)
	,Price INT
	)

SELECT D.* FROM Drivers D JOIN Source_Destination SD ON D.LocationId=SD.SourceId JOIN Riding R ON R.SrcDestId=SD.Id WHERE SD.Distance<6 AND R.Status<>'Started'AND D.LocationId=1